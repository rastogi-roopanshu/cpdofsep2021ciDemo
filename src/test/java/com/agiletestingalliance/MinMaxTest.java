package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void testCompareInt(){

        int k= new MinMax().f(4,3);
        assertEquals("Add", 4, k);

    }
   @Test
    public void testCompareScenario2() throws Exception {

        int k= new MinMax().f(3,4);
        assertEquals("Add", 4, k);

    }
    @Test
    public void testCompareScenario3() throws Exception {

        String k= new MinMax().bar("saurabh");
        assertEquals("bar", "saurabh", k);

String ka= new MinMax().bar("");
        assertEquals("bar", "", ka);
    }

     
}
