package com.agiletestingalliance;
import org.junit.Assert;
import org.junit.Test;
 
public class Testtest {
 
    @Test
    public void testWhenStringNull()  {
        com.agiletestingalliance.Test test=new com.agiletestingalliance.Test(null);
        Assert.assertNull(test.gstr());
    }
 
    @Test
    public void testWhenStringNotNull()  {
        com.agiletestingalliance.Test test=new com.agiletestingalliance.Test("NotNull");
        Assert.assertEquals("NotNull",test.gstr());
    }
 
    @Test
    public void testWhenStringEmpty()  {
        com.agiletestingalliance.Test test=new com.agiletestingalliance.Test("");
        Assert.assertEquals("",test.gstr());
    }
 
    @Test
    public void testWhenStringUpdated()  {
        com.agiletestingalliance.Test test=new com.agiletestingalliance.Test("");
        test.s="New Value";
        Assert.assertEquals("New Value",test.gstr());
    }
   
}
 
